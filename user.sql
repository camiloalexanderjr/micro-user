-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Servidor: moe-mysql-app: 3306
-- Tiempo de generación: 25-11-2021 a las 03:33:25
-- Versión del servidor: 5.7.36
-- Versión de PHP: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `moe_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth`
--

CREATE TABLE `auth` (
  `id` varchar(32) NOT NULL,
  `username` varchar(50) NOT NULL,
  `Password` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auth`
--

INSERT INTO `auth` (`id`, `username`, `Password`) VALUES
('eL9pGNH_7bSXfoaRDtv7j', 'RAfael12233', '$2b$05$qUT.UT7a/PjWZf3FyU2AJeMCUHI20Lpy4i6RVqmu/nsF1ouwOKPJK'),
('bS7bn3yaGG9cPvhf1be5v', 'Jhonerptte', '$2b$05$Ym2UYhUozhAGBpkUomvzZO5jC13saATjn48uRNZDN7011DrOiJkUq');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` varchar(32) NOT NULL,
  `name` varchar(32) NOT NULL,
  `username` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `name`, `username`) VALUES
('eL9pGNH_7bSXfoaRDtv7j', 'Rafael', 'RAfael12233'),
('bS7bn3yaGG9cPvhf1be5v', 'Jhoner', 'Jhonerptte');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
